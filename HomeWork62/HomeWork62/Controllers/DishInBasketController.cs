﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeWork62.Data;
using HomeWork62.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HomeWork62.Controllers
{
    public class DishInBasketController : Controller
    {
        private readonly ApplicationDbContext _context;


        public DishInBasketController(ApplicationDbContext context)
        {
            _context = context;
        }
        // GET: DishInBasket
        public ActionResult Index()
        {
            return View();
        }

        // GET: DishInBasket/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        public ActionResult Remove(int dishId, int basketId)
        {
            var CafeId = _context.Cafe.FirstOrDefault(x => x.BasketId == basketId).Id;

            var Exist = _context.DishInBasket.Any(x => x.DishId == dishId);

            if (!Exist)
            {
                return RedirectToAction("Details", "Cafe", new { @id = CafeId });
            }

            DishInBasket dishInBasket = _context.DishInBasket.FirstOrDefault( x => x.DishId == dishId);
            dishInBasket.CountDish -= 1;
            if (dishInBasket.CountDish == 0)
            {
                _context.Remove(dishInBasket);
            }
            else
            {
                _context.Update(dishInBasket);
               
            }
            _context.SaveChanges();

            return RedirectToAction("Details", "Cafe", new { @id = CafeId });
        }



            // GET: DishInBasket/Create
            public ActionResult Create(int dishId, int basketId)
        {
            var CafeId = _context.Cafe.FirstOrDefault(x => x.BasketId == basketId).Id;

            var exist = _context.DishInBasket.Any(x => x.DishId == dishId);
            if (exist)
            {
                var dishinbasket = _context.DishInBasket.Include(x => x.Dish).FirstOrDefault(x => x.DishId == dishId);
                dishinbasket.CountDish += 1;
                dishinbasket.SumDishInBasket = dishinbasket.CountDish * dishinbasket.Dish.Price;
                _context.Update(dishinbasket);
                _context.SaveChanges();
            }
            else
            {
                var dish = _context.Dishes.FirstOrDefault(x => x.Id == dishId);
                var basket = _context.Baskets.FirstOrDefault(x => x.Id == basketId);
                DishInBasket dishInBasket = new DishInBasket { DishId = dishId, BasketId = basketId, Dish = dish, Basket = basket, CountDish = 1 };
                dishInBasket.SumDishInBasket = dishInBasket.CountDish * dishInBasket.Dish.Price;
                _context.DishInBasket.Add(dishInBasket);
                _context.SaveChanges();
                
            }

            return RedirectToAction("Details", "Cafe", new { @id = CafeId });
        }

        // POST: DishInBasket/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: DishInBasket/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: DishInBasket/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: DishInBasket/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: DishInBasket/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}