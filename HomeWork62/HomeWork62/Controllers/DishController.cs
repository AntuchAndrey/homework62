﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeWork62.Data;
using HomeWork62.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HomeWork62.Controllers
{
    public class DishController : Controller
    {
        private readonly ApplicationDbContext _context;
        

        public DishController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Dish
        public ActionResult Index()
        {
            return View();
        }

        // GET: Dish/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Dish/Create
        public ActionResult Create(int id)
        {
            ViewBag.CafeId = id;
            ViewBag.CafeName = _context.Cafe.FirstOrDefault(x => x.Id == id).Name;
            return View();
        }

        // POST: Dish/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DishViewModel dishViewModel)
        {
            //try
            //{
            int i = Guid.NewGuid().GetHashCode();

            int j = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0);
            Cafe cafe = _context.Cafe.FirstOrDefault(x => x.Id == dishViewModel.CafeId);
            Dish dish = new Dish {/*Id = i,*/ Name = dishViewModel.Name, Description = dishViewModel.Description, Price = dishViewModel.Price, CafeId = dishViewModel.CafeId, Cafe = cafe};
                

                _context.Dishes.Add(dish);
                _context.SaveChanges();

                return RedirectToAction("Details", "Cafe", new { @id = dish.CafeId });
                //return RedirectToAction(nameof(Index));
            //}
            //catch
            //{
            //    return View();
            //}
        }

        // GET: Dish/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Dish/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Dish/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Dish/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}