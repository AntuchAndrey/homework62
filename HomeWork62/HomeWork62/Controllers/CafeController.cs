﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HomeWork62.Data;
using HomeWork62.Models;
using HomeWork62.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HomeWork62.Controllers
{
    
    public class CafeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment _environment;
        private readonly FileUploadService _fileUploadService;

        public CafeController(ApplicationDbContext context, IHostingEnvironment environment, FileUploadService fileUploadService)
        {
            _context = context;
            _environment = environment;
            _fileUploadService = fileUploadService;
        }

        // GET: Cafe
        public ActionResult Index()
        {
            var cafes = _context.Cafe;
            return View(cafes);
        }

        // GET: Cafe/Details/5
        public ActionResult Details(int id)
        {
            //Model.Basket.DishInBaskets
            var cafe = _context.Cafe.Include(x => x.Dishes).Include(x => x.Basket).ThenInclude(x => x.DishInBaskets).FirstOrDefault(x => x.Id == id);
            var DishInBasketInCurrentCafe = _context.DishInBasket.Where(x => x.BasketId == cafe.BasketId);
            foreach (var item in DishInBasketInCurrentCafe)
            {
                item.SumDishInBasket = item.CountDish * item.Dish.Price;
                cafe.Basket.Total += item.SumDishInBasket;
            }
            return View(cafe);
        }

        // GET: Cafe/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Cafe/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreatePost(CafeViewModel cafeViewModel)
        {
            //try
            //{
                Cafe cafe = new Cafe {Name = cafeViewModel.Name, ImageCafe = cafeViewModel.ImageCafe.FileName, Description = cafeViewModel.Description };

                var path = Path.Combine(
                   _environment.WebRootPath,
                   $"images\\{cafeViewModel.Name}\\image");
                _fileUploadService.Upload(path, cafeViewModel.ImageCafe.FileName, cafeViewModel.ImageCafe);
                cafe.ImageCafe = $"images/{cafeViewModel.Name}/image/{cafeViewModel.ImageCafe.FileName}";

                _context.Cafe.Add(cafe);
                _context.SaveChanges();

                Basket basket = new Basket { CafeId = cafe.Id, Cafe = cafe};

                _context.Baskets.Add(basket);
                _context.SaveChanges();

                cafe.BasketId = basket.Id;
                cafe.Basket = basket;
                _context.Update(cafe);
                _context.SaveChanges();

            return RedirectToAction(nameof(Index));
            //}
            //catch
            //{
            //    return View();
            //}
        }

        // GET: Cafe/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Cafe/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Cafe/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Cafe/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}