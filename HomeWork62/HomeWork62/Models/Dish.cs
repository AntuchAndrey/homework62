﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeWork62.Models
{
    public class Dish
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        
        public int CafeId { get; set; }
        public Cafe Cafe { get; set; }

        public DishInBasket DishInBasket { get; set; }
    }
}