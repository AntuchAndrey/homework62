﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HomeWork62.Models
{
    public class Cafe
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageCafe { get; set; }

        public int BasketId { get; set; }
        public Basket Basket { get; set; }

        public IEnumerable<Dish> Dishes { get; set; }
    }
}
