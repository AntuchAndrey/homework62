﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeWork62.Models
{
    public class Basket
    {
        public int Id { get; set; }

        public int CafeId { get; set; }
        public Cafe Cafe { get; set; }

        public double Total { get; set; }

        public IEnumerable<DishInBasket> DishInBaskets { get; set; }
    }
}
