﻿using System;
using System.Collections.Generic;
using System.Text;
using HomeWork62.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace HomeWork62.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<Cafe> Cafe { get; set; }
        public DbSet<Dish> Dishes { get; set; }
        public DbSet<Basket> Baskets { get; set; }
        public DbSet<DishInBasket> DishInBasket { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Cafe>()
               .HasMany(c => c.Dishes)
               .WithOne(c => c.Cafe)
               .HasPrincipalKey(p => p.Id);

            builder.Entity<Dish>()
                .HasOne(d => d.Cafe)
                .WithMany(d => d.Dishes)
                .HasForeignKey(d => d.CafeId);

            builder.Entity<Cafe>()
                .HasOne(a => a.Basket)
                .WithOne(b => b.Cafe)
                .HasForeignKey<Basket>(b => b.Id);

            builder.Entity<Basket>()
               .HasMany(c => c.DishInBaskets)
               .WithOne(c => c.Basket)
               .HasPrincipalKey(p => p.Id);

            builder.Entity<DishInBasket>()
                .HasOne(d => d.Basket)
                .WithMany(d => d.DishInBaskets)
                .HasForeignKey(d => d.BasketId)
                .OnDelete(DeleteBehavior.Restrict);

            //builder.Entity<DishInBasket>()
            //    .HasOne(a => a.Dish).
            //    .OwnsOne(a=>a.Dish)
            //   .HasForeignKey(b => b.Id);
               //.OnDelete(DeleteBehavior.Restrict);

        }
    }
}
